from PIL import Image
import sys
from turtle import *

with open('hpintro.txt', "r") as reader: #reads and prints entire file
    print(reader.read())
person = input(": ")  #allows user to imput something
if person == 'harry':
    with open('harry.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif person == 'hermione':
    with open('hermione.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
else:
    print("That is not an option.")

print("")
house = input(": ")  #allows user to imput something
if house == 'slytherin':
    with open('slytherin.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif house == 'griffindor':
    with open('griffindor.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif house == 'ravenclaw':
    with open('ravenclaw.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif house == 'hufflepuff':
    with open('hufflepuff.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
else:
    print("That is not an option.") #prints if not one of answers listed above

print("")
with open('move_stairs.txt', "r") as reader: #reads and prints entire file
    print(reader.read())
stairs = input(": ")  #allows user to imput something
if stairs == 'stay':
    with open('stay.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif stairs == 'jump':
    with open('jump.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif stairs == 'wand':
    with open('wand.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
else:
    print("That is not an option.") #prints if not one of answers listed above

print("")

actions = input(": ")  #allows user to imput something
if actions == 'team':
    with open('team.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif actions == 'magic':
    with open('magic.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
        myImage = Image.open('dog.jpg')     #this opens an image
        myImage.show()
        sys.exit()                         #exits program
elif actions == 'dance':
    with open('dance.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
        myImage = Image.open('dog.jpg')     #this opens an image
        myImage.show()
        sys.exit()                          #exits program
elif actions == 'sleep':
    with open('sleep.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif actions == 'fight':
    with open('fight.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
        myImage = Image.open('dog.jpg')     #this opens an image
        myImage.show()
        sys.exit()                          #exits program
elif actions == 'flute':
    with open('flute.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
else:
    print("That is not an option.") #prints if not one of answers listed above

print("")

with open('final.txt', "r") as reader: #reads and prints entire file
    print(reader.read())
final = input(": ")  #allows user to imput something
if final == 'red':
    with open('red.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
elif final == 'green':
    with open('green.txt', 'r') as reader: #reads and prints entire file
        print(reader.read())
        myImage = Image.open('end.jpg')          #this opens an image
        myImage.show()
else:
    print("That is not an option.") #prints if not one of answers listed above

print("")
sys.exit()
