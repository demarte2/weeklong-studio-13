
---
You wait it out until the stairs come back to the landing you need.
It could be 10 minutes or an hour, but that's ok.
You brought some butter beer and pumpkin bread to munch on.

The stairs return to their original position.
You move down the hall searching for your room.
Dumbledore just happens to be walking the hall as well.
You chat for a bit and to get out of the convo you open the door to your right.

Turns out.. its the door to the Three-Headed Dogs!
What do you do?

---
Team attack with magic: team
Lull the beast back to sleep: sleep